TUMSAMPLE=fastqs/H446
NORMSAMPLE=fastqs/NCC0018
TOTALREADS=20000000
OUTFOLDER=fastqsInSilico/Z018
OUTNAME=Z018_H446_NCC0018
PROPVALS=$(seq -s " " 0 0.0001 0.005)
PROPVALS2=$(echo 0.01 0.05 0.1)
PROPVALS=$(echo $PROPVALS $PROPVALS2)

cp ${TUMSAMPLE}* .
cp ${NORMSAMPLE}* .
gunzip *.fastq.gz

mkdir -p $OUTFOLDER

TUMSAMPLE=$(basename $TUMSAMPLE)
NORMSAMPLE=$(basename $NORMSAMPLE)

for PROP in ${PROPVALS}
do
echo $PROP

qsub -v TUMSAMPLE=${TUMSAMPLE},NORMSAMPLE=${NORMSAMPLE},PROP=${PROP},OUTNAME=${OUTNAME},OUTFOLDER=${OUTFOLDER},TOTALREADS=${TOTALREADS} <<-"EOF"

#!/bin/bash
#PBS -l nodes=1:ppn=1,mem=10gb
#PBS -l walltime=00:25:00:00
#PBS -j oe
#PBS -N mixReads

cd $PBS_O_WORKDIR

ml apps/fastqtools

mkdir -p $OUTFOLDER
mkdir -p temp${PROP}

TUMR1s=$(ls ./${TUMSAMPLE}*R1*)
NORMR1s=$(ls ./${NORMSAMPLE}*R1*)

NUMLANESTUM=$(ls ./${TUMSAMPLE}*MeCap*R1* | wc -w)
NUMLANESNORM=$(ls ./${NORMSAMPLE}*MeCap*R1* | wc -w)

echo $NUMLANESTUM
echo $NUMLANESNORM

TUMREADS=$(awk -v OFMT='%.0f' "BEGIN {x=${PROP}*${TOTALREADS}/${NUMLANESTUM}; print x}")
NORMREADS=$(awk -v OFMT='%.0f' "BEGIN {x=(1-${PROP})*${TOTALREADS}/${NUMLANESNORM}; print x}")

echo $TUMREADS
echo $NORMREADS

for TUMR1 in $TUMR1s
do

echo $TUMR1

TUMR2=$(echo $TUMR1 | sed "s/_R1_/_R2_/g")
SAMPLENAME=$(echo $TUMR1 | sed "s/_R1_.*//g")

REQREADS=$TUMREADS

if [[ "$TUMR1" =~ "Input" ]]; then
let REQREADS=$REQREADS/10;
fi

echo $REQREADS

fastq-sample -s 1771 -n $REQREADS -o temp${PROP}/${SAMPLENAME} $TUMR1 $TUMR2

done

for NORMR1 in $NORMR1s
do

echo $NORMR1

NORMR2=$(echo $NORMR1 | sed "s/R1/R2/g")
SAMPLENAME=$(echo $NORMR1 | sed "s/_R1.*//g")

REQREADS=$NORMREADS

if [[ "$NORMR1" =~ "Input" ]]; then
let REQREADS=$REQREADS/10;
fi

echo $REQREADS

fastq-sample -s 1771 -n $REQREADS -o temp${PROP}/${SAMPLENAME} $NORMR1 $NORMR2

done

PROPOUT=$(echo $PROP | sed "s/\\./p/g")

MECAPR1s=$(ls temp${PROP}/*MeCap*.1.*)
cat $MECAPR1s | gzip > ${OUTFOLDER}/${OUTNAME}_${PROPOUT}_MeCap_R1.fastq.gz

MECAPR2s=$(ls temp${PROP}/*MeCap*.2.*)
cat $MECAPR2s | gzip > ${OUTFOLDER}/${OUTNAME}_${PROPOUT}_MeCap_R2.fastq.gz

INPUTR1s=$(ls temp${PROP}/*Input*.1.*)
cat $INPUTR1s | gzip > ${OUTFOLDER}/${OUTNAME}_${PROPOUT}_Input_R1.fastq.gz

INPUTR2s=$(ls temp${PROP}/*Input*.2.*)
cat $INPUTR2s | gzip > ${OUTFOLDER}/${OUTNAME}_${PROPOUT}_Input_R2.fastq.gz

EOF

done
