#' CpG islands for hg38.
#'
#' A data.table/GRanges object containing genome mappability per 10kb window on the genome.
#' Downloaded from ichorCNA,  https://github.com/broadinstitute/ichorCNA, formatted for hmmcopy.
#' Note chromosomes are in size order, not numerical order.
#'
#'
#' @format A data.table object with 308837 ranges and 4 columns
#' @export
#'
#' \describe{
#'   \item{chr}{chromosome}
#'   \item{start}{window start position}
#'   \item{end}{window end position}
#'   \item{gc}{mappability score of the 10kb window}
#'   ...
#' }
"map_hg38_10kb"
