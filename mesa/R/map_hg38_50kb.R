#' CpG islands for hg38.
#'
#' A data.table/GRanges object containing genome mappability per 50kb window on the genome.
#' Downloaded from ichorCNA,  https://github.com/broadinstitute/ichorCNA, formatted for hmmcopy.
#' Note chromosomes are in size order, not numerical order.
#'
#'
#' @format A data.table object with 61775 ranges and 4 columns
#' @export
#'
#' \describe{
#'   \item{chr}{chromosome}
#'   \item{start}{window start position}
#'   \item{end}{window end position}
#'   \item{gc}{mappability score of the 50kb window}
#'   ...
#' }
"map_hg38_50kb"
