#' CpG islands for CGPatternGRCh38
#'
#' A GRanges object containing the location of CG dinucleotides in GRCh38 coordinates
#'
#'
#' @format A GRanges object with 27852739 ranges and 0 metadata columns:
#'
#' \describe{
#'   ...
#' }
"CGPatternGRCh38"
