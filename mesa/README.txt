An R package for analysis of the methylation pulldown sequencing data, mostly built on top of the qsea package.
Includes a number of convenience functions, as well as some useful data objects.
