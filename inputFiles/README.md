The following files on the [Zenodo repository](https://doi.org/10.5281/zenodo.5569260) should be downloaded and placed in this directory in order to run the analysis code:

CDXarrayWide.csv  
infinium-methylationepic-v-1-0-b5-manifest-file.csv  
KeyTFsIncYAP.csv  
SCLC transcript expression adjusted for batch effects.csv  
SCLC_methylation_beta_values_of_individual_probes_after_QC_and_filtering_out_SNVs.csv  
poirier_oncogene.rda  
PoirierEtAl_Oncogene2015_SuppTab1.csv  
PoirierEtAl_Oncogene2015_SuppTab2.csv
