# SCLC cfDNA Methylome Profiling

This repository contains code to reproduce the analysis in the paper:

**Chemi\*, Pearce\* et al. cfDNA methylome profiling for detection and subtyping of Small Cell Lung Cancers. *Nature Cancer* (in press).**  

*Equal contributions

## Raw data

Raw FASTQ files have been deposited in EGA under the accession number [EGAS00001005739](https://ega-archive.org/studies/EGAS00001005739) (Data Access Request required).

## Processed data

The processed data used for the analysis mainly consists of R objects (qseaSets) from the [qsea package](https://bioconductor.org/packages/release/bioc/html/qsea.html), which contain the methylation read counts of each sample across 300bp windows of the human genome.

The processed data can be obtained at [this Zenodo repository](https://doi.org/10.5281/zenodo.5569260) (Data Access Request required).

**Most of the code here will not run on its own, without these objects.**

## Code

### mesa R package

The `mesa` directory consists of an R package used throughout the code, and can be loaded with devtools:
```
devtools::load_all("mesa")
```
This is the version of `mesa` as used in this analysis. The latest version can be found at [this GitHub repository](https://github.com/cruk-mi/mesa/).

### Generation of qseaSets

The qseaSets were generated from the raw FASTQ files using the Nextflow pipeline included in the directory `0-qsetPipeline`. These were initially processed as individual sequencing NextSeq/NovaSeq runs, before being combined together in R (code in directory `1-setup`). 

The sampleTables in the `0-qsetPipeline` should work from the raw data with the names associated to the samples there.
<br/><br/>
<br/><br/>

| :exclamation:  **The rest of the code detailed below can currently be found at [this Zenodo repository](https://doi.org/10.5281/zenodo.5569260)** (Data Access Request required), together with the qseaSets required to run the code.   |
|-----------------------------------------|

<br/><br/>

### Generation of mixture sets

"Mixture sets" consisting of *in silico* spike-ins of small amounts of reads from tumour samples combined with non-cancer control cfDNA were generated using code in directory `2-makeMixSets`. This outputs qseaSets which can also be found on the [Zenodo repository](https://doi.org/10.5281/zenodo.5569260).

### Data analysis

The code in directory `3-general` includes some comparisons of matched CDX and cfDNA samples, as well as between MBDseq and Infinium 450k arrays. It also generates the differentially methylated regions (DMRs) used to pick windows for the tumour/normal classifier, and performs the PCAs.

Code for the machine learning ensemble classifiers detailed in the paper is in directory `4-classifiers`, which includes making predictions on the held-out training sets and validation sets, as well as the setting of cutoffs. 

Code related to the methylation score and survival analysis is in directory `5-methylationScore`.

Code for the prediction of SCLC subtype in paired pre- and post-treatment samples is in directory `6-postTreatment`.

## Instructions to reproduce the data analysis

- Download this repository and set it as the working directory in R.
- Download the qseaSets from the [Zenodo repository](https://doi.org/10.5281/zenodo.5569260) and put them in the `qseaSets` directory. See the [README file](qseaSets/README.md) in the `qseaSets` directory for more details.
- Download other required input files from the [Zenodo repository](https://doi.org/10.5281/zenodo.5569260) and put them in the `inputFiles` directory. See the [README file](inputFiles/README.md) in the `inputFiles` directory for more details.
- Replace the directories `2-makeMixSets`, `3-general`, `4-classifiers`, `5-methylationScore` and `6-postTreatment` in this repository with those downloaded from the [Zenodo repository](https://doi.org/10.5281/zenodo.5569260).
- Install required R packages.
- Knit the following Rmd files:
```
3-general/calculateDMRsForClassifier.Rmd
3-general/compareArrayQset.Rmd
3-general/compareDXandcfDNA.Rmd
3-general/compareDNAinputAmounts.Rmd
3-general/comparePoirierArrays.Rmd
3-general/subtypePCA.Rmd
3-general/initialDMRs.Rmd
4-classifiers/subtypeClassifier.Rmd
4-classifiers/TumourNormalClassifier.Rmd
5-methylationScore/SCLCsurvival.Rmd
6-postTreatment/matchedSamples.Rmd
```
The primary outputs of the code are as follows:
- Plots that appear in the paper are saved to directory `plots`
- Figure source data for the paper are saved to directory `files/figureSourceData`
- DMRs, classifier input regions, models and predictions are saved to directories `files/TNclassifier` and `files/SubtypeClassifier`
- html reports for each Rmd file.

The above output directories are automatically created by the code if they do not already exist.
