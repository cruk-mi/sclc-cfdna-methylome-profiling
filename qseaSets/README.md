The following qseaSets on the [Zenodo repository](https://doi.org/10.5281/zenodo.5569260) should be downloaded and placed in this directory in order to run the analysis code:

ArrayCDXs_percent100.rds  
CellLine_100percent.rds  
Cellline_mixtureSets.rds  
cfDNA_All_min50_max1000_w300_q10.rds  
cfDNApostTreatment_All_min50_max1000_w300_q10.rds  
DilutionSeries_CDX13_min50_max1000_w300_q10.rds  
DilutionSeries_CDX29_min50_max1000_w300_q10.rds  
DilutionSeries_CDX32_min50_max1000_w300_q10.rds  
DilutionSeries_H446_Rep*_min50_max1000_w300_q10.rds (11 files)  
DX_All_min50_max1000_w300_q10.rds  
DX_merged.rds  
NCCsForTrain_All_min50_max1000_w300_q10.rds  
TNmixSets_combined_regionsFiltered_redo2.rds  
ValidationSet_All_min50_max1000_w300_q10.rds  
varyDNAinput_All_min50_max1000_w300_q10.rds 

