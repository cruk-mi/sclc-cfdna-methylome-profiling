#!/usr/bin/env nextflow

// Create a channel for input read files.

Channel
.fromFilePairs( params.reads )
.ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}\nNB: Path needs to be enclosed in quotes!" }
.set { ch_raw_reads }

if ( params.qseaSampleInfo ) {
qseaSampleInfo = Channel
.fromPath( params.qseaSampleInfo )
.set { ch_qsea_sampleInfo }
}

ch_raw_reads
.into { ch_cutfastq_files_1; ch_cutfastq_files_2; ch_cutfastq_files_qc; ch_cutfastq_files_3 }

process fastq_screen {
  publishDir "${params.outDir}/fastq_screen"
  label 'single_core_medium'

  input:
    set val(name), file(reads) from ch_cutfastq_files_3

  output:
    file "*" into fastq_screen_results

  script:
    """
    fastq_screen --conf ${params.fastqScreenConfig} --aligner bowtie2 ${reads}
    """
}


process fastq_qc_adapters_cutting {
  publishDir "${params.outDir}/fastqc_fastq"
  label 'single_core'

  input:
    set val(name), file(reads) from ch_cutfastq_files_qc

  output:
    file "*_fastqc.{zip,html}" into fastqc_results_adapters_cutting

  script:
    """
    fastqc -q ${reads}
    """
}

/*
  * STEP 5 - Mapping sequences against the reference genome.
*/

  process bam_mapping {
    label 'full_node'
    echo false
    publishDir "$params.outDir/bam_mapping"

    input:
      set val(name), file(reads) from ch_cutfastq_files_1

    output:
      file("*.bam") into ch_aligned_mapped

    script:
    rg = "\'@RG\\tID:${name}\\tSM:${name}\\tPL:ILLUMINA\\tLB:${name}\\tPU:1\'"

    """
  bwa mem -M -t ${task.cpus} -R ${rg} ${params.genome} ${reads} |
  samtools view -b -@ ${task.cpus} - |
  samtools sort -n -@ ${task.cpus} -o ${name}_aligned.bam

  """
  }

// Create a duplicated channel for mapped human bam files.

ch_aligned_mapped.into {ch_aligned_mapped_files_1; ch_aligned_mapped_files_2; ch_aligned_mapped_files_3}

/*
  * STEP 6 - Merging individual lanes to a single bam file.
*/

ch_aligned_mapped_files_2
.flatten()
.map {file -> tuple(file.name.toString().replaceAll('_S\\d{1,2}_L','_L').replaceAll('_L\\d\\d\\d_','_'), file) }
.groupTuple(by:0)
.into { ch_aligned_mapped_merging; ch_aligned_mapped_merging2; ch_aligned_mapped_files_qc }

process bam_merged {
  label 'quarter_node'
  echo false
  publishDir "$params.outDir/bam_merged"

  input:
    set val(name), file(reads) from ch_aligned_mapped_merging

  output:
    file("*.bam") into ch_aligned_merged

  script:

  """
  samtools merge -n -@ ${task.cpus} - $reads | samtools sort -n -@ ${task.cpus} -o ${name}.bam -
  """
}


ch_aligned_merged.into{ ch_aligned_merged_files_1; ch_aligned_merged_files_2; ch_aligned_merged_files_3; ch_aligned_merged_files_4 }

// Create a channel of merged human bams for qc

ch_aligned_merged_files_1
.flatten()
.map {file -> tuple(file.name.toString().split('_aligned').first(), file) }
.groupTuple(by:0)
.into { ch_aligned_merged_qc; ch_aligned_merged_flagstat }

process flagstat_aligned {
  label 'single_core'
  publishDir "$params.outDir/flagstat_results"

  input:
    set val(name), file(reads) from ch_aligned_merged_flagstat

  output:
    tuple val(name), path("*.sorted.{bam,bam.bai}"), path("*.flagstat") into ch_sort_bam_aligned
    path "*.{flagstat,idxstats,stats}" into flagstat_results_aligned

  script:
    """
    samtools sort -@ $task.cpus -o ${name}.sorted.bam -T $name $reads
    samtools index ${name}.sorted.bam
    samtools flagstat ${name}.sorted.bam > ${name}_aligned.sorted.bam.flagstat
    samtools idxstats ${name}.sorted.bam > ${name}_aligned.sorted.bam.idxstats
    samtools stats ${name}.sorted.bam > ${name}_aligned.sorted.bam.stats
    """
}

process fastq_qc {
  label 'single_core'
  input:
    set val(name), file(reads) from ch_aligned_merged_qc

  output:
    file "*_fastqc.{zip,html}" into fastqc_results

  script:
    """
    fastqc -q ${reads}
    """
}

/*
  * STEP 7 - (optional) Filtering of contamination genome from reads genomic profile.
*/

  // Mapping low-divergent sequences against a contamination reference genome.

process contamination_mapping {
  label 'full_node'
  echo false
  publishDir "$params.outDir/contamination_mapping"

  input:
    set val(name), file(reads) from ch_cutfastq_files_2
    file(params.contaminationGenome)

  output:
    file("*.bam") into ch_contamination_mapped

  when:
    params.contamination

  script:

  """
  bwa mem -M -t ${task.cpus} ${params.contaminationGenome} ${reads} |
  samtools view -b -@ ${task.cpus} - |
  samtools sort -n -@ ${task.cpus} -o ${name}_contamination.bam
  """
}


// Create a channel for mapped contamination bam files.

ch_contamination_mapped
.flatten()
.map {file -> tuple(file.name.toString().replaceAll('_S\\d{1,2}_L','_L').replaceAll('_L0[0-9][0-9]_','_'), file) }
.groupTuple(by:0)
.into { ch_contamination_mapped_merging; ch_contamination_mapped_merging2 }

process contamination_merging {
  label 'quarter_node'
  echo true
  publishDir "$params.outDir/contamination_merged"

  input:
    set val(name), file(reads) from ch_contamination_mapped_merging

  output:
    file("*.bam") into ch_merged_contamination

  when:
    params.contamination

  script:

  """
  samtools merge -n -@ ${task.cpus} - $reads | samtools sort -n -@ ${task.cpus} -o ${name} -
  """
}


ch_merged_contamination.into{ ch_merged_contamination_1; ch_merged_contamination_2 }

// Create a channel of merged human bams for qc

ch_merged_contamination_1
.flatten()
.map {file -> tuple(file.name.toString().split('_contamination').first(), file) }
.groupTuple(by:0)
.set { ch_contamination_merged_qc }

process fastq_qc_contamination_merging {
  label 'single_core'

  input:
    set val(name), file(reads) from ch_contamination_merged_qc

  output:
    file "*_fastqc.{zip,html}" into fastqc_results_contamination_merging

  script:
    """
    fastqc -q ${reads}
    """
}

// Filtering of contamination genome from reads genomic profile.

ch_aligned_merged_files_2.flatten()
.concat(ch_merged_contamination_2.flatten())
.map {file -> tuple(file.name.toString().split('_contamination.bam|_aligned.bam').first(), file) }
.groupTuple(by:0)
.set {joined_channel}

process contamination_filtering {
  echo false
  label 'single_core'
  publishDir "$params.outDir/contamination_filtering"

  input:
    set val(name), file(reads) from joined_channel

  output:
    file("*.bam") into ch_bams_filtered

  when:
    params.contamination

  script:

    """
  bamcmp -s "as" ${reads} -1 ${reads[0]} -2 ${reads[1]} -A ${name}_contaminationFiltered.bam
  """
}

// Create a channel of merged bams for deduplication.

if(params.contamination){
  ch_bams_filtered
  .flatten()
  .map {file -> tuple(file.name.toString().split('_contaminationFiltered').first(), file) }
  .groupTuple(by:0)
  .into { ch_merged_bams_files; ch_merged_bams_files_qc; ch_merged_bams_files_flagstat }

} else {

  ch_aligned_merged_files_3
  .flatten()
  .map {file -> tuple(file.name.toString().split('_aligned').first(), file) }
  .groupTuple(by:0)
  .into { ch_merged_bams_files; ch_merged_bams_files_qc; ch_merged_bams_files_flagstat }
}

process flagstat_contamination {
  label 'single_core'
  publishDir "$params.outDir/flagstat_results"

  input:
    set val(name), file(reads) from ch_merged_bams_files_flagstat

  output:
    tuple val(name), path("*.sorted.{bam,bam.bai}"), path("*.flagstat") into ch_sort_bam_contamination
    path "*.{flagstat,idxstats,stats}" into flagstat_results_contamination

  script:
    """
    samtools sort -@ $task.cpus -o ${name}.sorted.bam -T $name $reads
    samtools index ${name}.sorted.bam
    samtools flagstat ${name}.sorted.bam > ${name}_contamination.sorted.bam.flagstat
    samtools idxstats ${name}.sorted.bam > ${name}_contamination.sorted.bam.idxstats
    samtools stats ${name}.sorted.bam > ${name}_contamination.sorted.bam.stats
    """
}

process fastq_qc_contamination_filtered {
  label 'single_core'

  input:
    set val(name), file(reads) from ch_merged_bams_files_qc

  output:
    file "*_fastqc.{zip,html}" into fastqc_results_contamination_filtered

  when:
    params.contamination

  script:
    """
    fastqc -q ${reads}
    """
}

process bams_sort {
  label 'single_core_medium'
  echo false
  publishDir "$params.outDir/bams_sort"

  input:
    set val(name), file(reads) from ch_merged_bams_files

  output:
    file("*.bam") into ch_bams_sort

  script:

    """
  samtools sort -@ ${task.cpus} -o ${name}.bam ${reads}
  """
}

ch_bams_sort
.flatten()
.map {file -> tuple(file.name.toString().split('.bam').first(), file) }
.groupTuple(by:0)
.set { ch_bams_sort_files }


/*
  * STEP 8 - Deduplicating reads using umi tools dedup as an option.
*/

  process bams_deduplicating {
    label 'single_core_medium'
    echo false
    publishDir "$params.outDir/bams_deduplicating"

    input:
      set val(name), file(reads) from ch_bams_sort_files

    output:
      file("*dedup.bam") into ch_dedup_bams

    script:

script:
   if( params.umi )
   """
  samtools index -@ ${task.cpus} ${reads}
  umi_tools dedup --paired --chimeric-pairs=discard --ignore-tlen --stdin=${reads} --log=${name}_dedup.log |
    samtools sort -@ ${task.cpus} -o ${name}_dedup.bam
  samtools index -@ ${task.cpus} ${name}_dedup.bam
   """
   else
   """
  samtools index -@ ${task.cpus} ${reads}
  umi_tools dedup --paired --chimeric-pairs=discard --ignore-tlen --stdin=${reads} --log=${name}_dedup.log |
    samtools sort -@ ${task.cpus} -o ${name}_dedup.bam
  samtools index -@ ${task.cpus} ${name}_dedup.bam

   """
  }

ch_dedup_bams.into { ch_dedup_bams_filtering; ch_dedup_bams_fixmate; ch_dedup_bams_qc }

// Create a channel of dedup bams for qc

ch_dedup_bams_qc
.flatten()
.map {file -> tuple(file.name.toString().split('_dedup').first(), file) }
.groupTuple(by:0)
.into { ch_dedup_qc; ch_dedup_flagstat }

process flagstat_dedup {
  label 'single_core'
  publishDir "$params.outDir/flagstat_results"

  input:
    set val(name), file(reads) from ch_dedup_flagstat

  output:
    tuple val(name), path("*.sorted.{bam,bam.bai}"), path("*.flagstat") into ch_sort_bam_dedup
  path "*.{flagstat,idxstats,stats}" into flagstat_results_dedup

  script:
    """
    samtools sort -@ $task.cpus -o ${name}.sorted.bam -T $name $reads
    samtools index ${name}.sorted.bam
    samtools flagstat ${name}.sorted.bam > ${name}_dedup.sorted.bam.flagstat
    samtools idxstats ${name}.sorted.bam > ${name}_dedup.sorted.bam.idxstats
    samtools stats ${name}.sorted.bam > ${name}_dedup.sorted.bam.stats
    """
}

process fastq_qc_bams_deduplicating {
  label 'single_core'

  input:
    set val(name), file(reads) from ch_dedup_qc

  output:
    file "*_fastqc.{zip,html}" into fastqc_results_bams_deduplicating

  script:
    """

    fastqc -q ${reads}
    """
}

/*
  * STEP 10 - Fixmate reads.
*/

  process read_fixmate {
    label 'quarter_node'
    echo false
    publishDir "$params.outDir/bams_fixmate"
    publishDir "$params.publishDir/PipelineBams", mode: 'copy'

    input:
      file(bam) from ch_dedup_bams_fixmate

    output:
      file("*.bam") into ch_final_bams
      file("*.bam.bai") into ch_final_bai

    script:

      """

  samtools sort -@ ${task.cpus} -n ${bam} |
  samtools fixmate -@ ${task.cpus} -r -c -m - - |
  samtools sort -@ ${task.cpus} - > ${bam.toString().split('_dedup').first()}.bam
  samtools index ${bam.toString().split('_dedup').first()}.bam

  """
  }


ch_final_bams.into{ch_final_bams_0; ch_final_bams_1; ch_final_bams_2; ch_final_bams_3; ch_final_bams_4 }


// Create a channel of final bams for qc

ch_final_bams_1
.flatten()
.map {file -> tuple(file.name.toString().split('.bam').first(), file) }
.groupTuple(by:0)
.set { ch_final_bams_files_qc }

process fastq_qc_read_filtering {
  label 'single_core'

  input:
    set val(name), file(reads) from ch_final_bams_files_qc

  output:
    file "*_fastqc.{zip,html}" into fastqc_results_read_filtering

  script:
    """
    fastqc -q ${reads}
    """
}

// Launch the NGSCheckMate pipeline on the final bams folder

ch_final_bams_2.flatten()
.concat(ch_final_bai.flatten())
.map {file -> tuple(file.name.toString().split('.bam|.bam.bai').first(), file) }
.groupTuple(by:0)
.into { ch_final_bams_files_1; ch_final_bams_files_2 }

process vcf_calling{
  echo false
  publishDir "$params.outDir/ngscheckmate_vcf"
  publishDir "$params.publishDir/NGSCheckMateOutput", mode: 'copy'

  input:
  file genome from file(params.genome)
  file snp from file(params.ngsCheckmateBed)
  set val(name), file(bam) from ch_final_bams_files_1

  output:
    file("*.vcf") into vcf_ch

  """
  samtools faidx ${genome}
	grep -v "alt" ${snp} > SNP.bed

  bcftools mpileup --max-depth 5000 --ignore-RG -Ou -I -R SNP.bed -f ${genome} ${bam[0]} |
  bcftools call -c | bcftools sort -Ou | bcftools norm -d none -O v -o ${name}.vcf

  """
}

process ngscheckmate_run {

  errorStrategy 'ignore'
  publishDir "$params.outDir/ngscheckmate_output"
  publishDir "$params.publishDir/NGSCheckMateOutput", mode: 'copy'

  input:
    file (vcf) from vcf_ch.collect()
    file snp from file(params.ngsCheckmateBed)

  output:
    file ("*") into ncm_ch

  script:
    """

	grep -v "alt" ${snp} > SNP.bed
  ls -d "\${PWD}"/*.vcf > vcfList.txt

	python ncm.py -V -l vcfList.txt -bed SNP.bed -O .
	"""
}



//Sample Table
ch_aligned_merged_files_4
.flatten()
.map {file -> tuple(file.name.toString().split('_aligned').first(), file) }
.groupTuple(by:0)
.set { ch_merged_bams_tab }

/*
  * STEP 11 - Qualimap
*/
  process qualimap {
    tag "$name"
    publishDir "${params.outDir}/qualimap_results", mode: 'copy'

    input:
      set val(name), file(bam) from ch_merged_bams_tab

    output:
      file "${bam.baseName}_qualimap" into ch_qualimap_results_for_multiqc

    script:
    gcref = params.genome.toString().startsWith('GRCh') ? '-gd HUMAN' : ''
    gcref = params.genome.toString().startsWith('GRCm') ? '-gd MOUSE' : ''
    def avail_mem = task.memory ? ((task.memory.toGiga() - 6) / task.cpus).trunc() : false
    def sort_mem = avail_mem && avail_mem > 2 ? "-m ${avail_mem}G" : ''
    """
    samtools sort $bam \\
        -@ ${task.cpus} $sort_mem \\
        -o ${bam.baseName}.sorted.bam

    qualimap bamqc $gcref \\
        -bam ${bam.baseName}.sorted.bam \\
        --java-mem-size=4G \\
        -outdir ${bam.baseName}_qualimap \\
        --collect-overlap-pairs
    """
  }

/*
  * STEP 12 - Produce a summary information report.
*/
  process fastq_multiqc {
    label 'single_core'
    publishDir "${params.outDir}/fastq_multiqc"
    publishDir "$params.publishDir/fastqMultiQC", mode: 'copy'

    input:
    file ('fastqc_fastq/*') from fastqc_results_adapters_cutting.collect().ifEmpty([])
    file ('fastq_screen/*') from fastq_screen_results.collect().ifEmpty([])

    output:
    file "multiqc_report.html" into multiqc_report_fastq
    file "multiqc_data"

    script:
      """
    multiqc .
    """
  }

process bams_multiqc {
  label 'single_core'
  publishDir "${params.outDir}/bams_multiqc"
  publishDir "$params.publishDir/bamsMultiQC", mode: 'copy'

  input:
  //file ('fastqc/*') from fastqc_results_read_filtering.collect().ifEmpty([])
  //file ('fastqc/*') from fastqc_results_bams_deduplicating.collect().ifEmpty([])
  //file ('fastqc/*') from fastqc_results_contamination_merging.collect().ifEmpty([])
  //file ('fastqc/*') from fastqc_results.collect().ifEmpty([])
  //file ('fastqc/*') from fastqc_results_contamination_filtered.collect().ifEmpty([])
  file ('flagstat_results/*') from flagstat_results_aligned.collect().ifEmpty([])
  file ('flagstat_results/*') from flagstat_results_contamination.collect().ifEmpty([])
  file ('flagstat_results/*') from flagstat_results_dedup.collect().ifEmpty([])
  //file ('demultiplexing/*') from ch_split_logs.collect().ifEmpty([])
  file ('qualimap_results/*') from ch_qualimap_results_for_multiqc.collect().ifEmpty([])

  output:
  file "multiqc_report.html" into multiqc_report_bams
  file "multiqc_data/multiqc_samtools_flagstat.txt" into multiqc_flagstat_bams

  script:
    """
    multiqc .
    """
}


process runIchorCNA {
  label 'single_core_medium'
  errorStrategy 'ignore'
  publishDir "$params.outDir/ichorCNA"
  publishDir "$params.publishDir/ichorCNA", mode: 'copy'

  input:
    file(bam) from ch_final_bams_3
    each file(gcwig) from Channel.fromPath("$workflow.projectDir/assets/gc_hg38_1000kb.wig")
    each file(mapwig) from Channel.fromPath("$workflow.projectDir/assets/map_hg38_1000kb.wig")
    each file(centromere) from Channel.fromPath("$workflow.projectDir/assets/GRCh38.GCA_000001405.2_centromere_acen.txt")
    each file(panelOfNormals) from Channel.fromPath("$workflow.projectDir/assets/ichorPoN_uptoM033_1000kb_median.rds")

  output:
    file "*"
    file "*.params.txt" into ch_ichor_params

  script:
    """

    samtools index ${bam}

    readCounter --window ${params.cnvSize} --quality 10 \
      --chromosome "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y" \
      ${bam} > ${bam.name}.wig

    runIchorCNA.R --id ${bam.name.toString().split('\\.bam').first()} \
      --WIG ${bam.name}.wig --ploidy "c(2)" --normal "c(0.5, 0.75, 0.9, 0.99, 0.995, 0.999, 1)" --maxCN 5 \
      --gcWig $gcwig \
      --mapWig $mapwig \
      --centromere $centromere \
      --normalPanel $panelOfNormals \
      --includeHOMD TRUE --chrs "c(1:22)" --chrTrain "c(1:22)" --genomeBuild "hg38" \
      --estimateNormal True --estimatePloidy True --estimateScPrevalence FALSE \
      --scStates "c()" --txnE 0.9999 --txnStrength 10000 --outDir .

    """
}


process reads_stats {
  label 'single_core'
  publishDir "${params.outDir}"
  publishDir "$params.publishDir", mode: 'copy'

  input:
  file (stats) from multiqc_flagstat_bams
  file(ichorParams) from ch_ichor_params.collect()

  output:
  file("*_readStats.csv") into ch_read_stats
  file("*_ichorStats.csv") into ch_ichor_stats

  script:
    """
  readStats.R ${stats} ${params.experimentName}_readStats.csv ${params.experimentName}_ichorStats.csv
    """
}

ch_maxInsertSize = Channel.from(160,-1)
ch_windowSize = Channel.from(300)


if ( params.qseaPooledControlMeCap ) {
Channel
.fromPath( params.qseaPooledControlMeCap )
.set { ch_qsea_PooledControlMeCap }
}

if ( params.qseaPooledControlInput ) {
Channel
.fromPath( params.qseaPooledControlInput )
.set { ch_qsea_PooledControlInput }
}

if ( params.qseaBadRegionsBed ) {
Channel
.fromPath( params.qseaBadRegionsBed )
.set { ch_qsea_BadRegionsBed }
}

process run_qsea {
  label 'large_node'
  publishDir "${params.outDir}/qsea"
  publishDir "${params.publishDir}/qseaSets", mode: 'copy'
  echo true

  input:
    file(bams) from ch_final_bams_0.collect()
    file(sampleInfo) from ch_qsea_sampleInfo
    file(pooledControlMeCap) from ch_qsea_PooledControlMeCap
    file(pooledControlInput) from ch_qsea_PooledControlInput
    file(badRegionsBed) from ch_qsea_BadRegionsBed
    file(readStats) from ch_read_stats
    file(ichorStats) from ch_ichor_stats
    each maxInsertSize from ch_maxInsertSize
    each windowSize from ch_windowSize

  output:
    file "*"
    file "*.rds" into ch_qsea_output


  script:
   if( maxInsertSize == -1 )
   """
    samtools index ${pooledControlMeCap}
    samtools index ${pooledControlInput}

    makeQset.R -n ${task.cpus} -y 50 \
     -z 1000 -w ${windowSize} \
     -t ${params.qseaFragmentType} -e ${params.experimentName} \
     -s ${sampleInfo} -r ${readStats} -q ${ichorStats} -m ${params.contamination} \
     --pooledcontrolmecap ${pooledControlMeCap} \
     --pooledcontrolinput ${pooledControlInput} \
     --badregions2 ${badRegionsBed} \
     --cnvwindowsize ${params.cnvSize}

   """
   else
   """
    samtools index ${pooledControlMeCap}
    samtools index ${pooledControlInput}

    makeQset.R -n ${task.cpus} -y 90 \
     -z ${maxInsertSize} -w ${windowSize} \
     -t ${params.qseaFragmentType} -e ${params.experimentName} \
     -s ${sampleInfo} -r ${readStats} -q ${ichorStats} -m ${params.contamination} \
     --pooledcontrolmecap ${pooledControlMeCap} \
     --pooledcontrolinput ${pooledControlInput} \
     --badregions2 ${badRegionsBed} \
     --cnvwindowsize ${params.cnvSize}
   """
}

process get_endmers {
  publishDir "${params.outDir}/endmerDist"
  publishDir "${params.publishDir}/endmerDist", mode: 'copy'
  label 'single_core'

  input:
    file(bam) from ch_final_bams_4

  output:
    file "*.txt"

  script:
    """

    samtools fasta -f 64 ${bam} | grep -v ">" | cut -c1-5 | sort | uniq -c > ${bam.baseName}_endmerDist.txt

    """
}

process methtools_report {
  publishDir "${params.outDir}"
  publishDir "${params.publishDir}", mode: 'copy'
  label 'full_node'

  input:
    file(reads) from ch_qsea_output.collect()
    file(initialReport) from Channel.fromPath("$workflow.projectDir/assets/initialReport.Rmd")

  output:
    file "*"

  script:
    """
    cp -L initialReport.Rmd ${params.experimentName}_initialReport.Rmd
    Rscript -e "rmarkdown::render('${params.experimentName}_initialReport.Rmd')"

    """
}
