ml apps/hmmcopyutils/0.99.0

gcCounter -w 10000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_10kb.wig
gcCounter -w 20000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_20kb.wig
gcCounter -w 30000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_30kb.wig
gcCounter -w 40000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_40kb.wig
gcCounter -w 50000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_50kb.wig
gcCounter -w 60000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_60kb.wig
gcCounter -w 70000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_70kb.wig
gcCounter -w 80000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_80kb.wig
gcCounter -w 90000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_90kb.wig
gcCounter -w 100000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_100kb.wig
gcCounter -w 150000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_150kb.wig
gcCounter -w 200000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_200kb.wig
gcCounter -w 300000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_300kb.wig
gcCounter -w 400000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_400kb.wig
gcCounter -w 500000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_500kb.wig
gcCounter -w 600000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_600kb.wig
gcCounter -w 700000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_700kb.wig
gcCounter -w 800000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_800kb.wig
gcCounter -w 900000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_900kb.wig
gcCounter -w 1000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_1000kb.wig
gcCounter -w 2000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_2000kb.wig
gcCounter -w 5000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_5000kb.wig
gcCounter -w 10000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_10000kb.wig
gcCounter -w 20000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_20000kb.wig

gcCounter -w 10000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_10kb_f.wig
gcCounter -w 20000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_20kb_f.wig
gcCounter -w 30000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_30kb_f.wig
gcCounter -w 40000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_40kb_f.wig
gcCounter -w 50000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_50kb_f.wig
gcCounter -w 60000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_60kb_f.wig
gcCounter -w 70000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_70kb_f.wig
gcCounter -w 80000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_80kb_f.wig
gcCounter -w 90000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_90kb_f.wig
gcCounter -w 100000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_100kb_f.wig
gcCounter -w 150000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_150kb_f.wig
gcCounter -w 200000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_200kb_f.wig
gcCounter -w 300000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_300kb_f.wig
gcCounter -w 400000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_400kb_f.wig
gcCounter -w 500000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_500kb_f.wig
gcCounter -w 600000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_600kb_f.wig
gcCounter -w 700000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_700kb_f.wig
gcCounter -w 800000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_800kb_f.wig
gcCounter -w 900000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_900kb_f.wig
gcCounter -w 1000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_1000kb_f.wig
gcCounter -w 2000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_2000kb_f.wig
gcCounter -w 5000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_5000kb_f.wig
gcCounter -w 10000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_10000kb_f.wig
gcCounter -w 20000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_20000kb_f.wig
gcCounter -w 40000000 -f -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y /data/cep/kmurat/genome.fa > gc_hg38_40000kb_f.wig



cp /data/cep/kmurat/genome.fa .
ml apps/hmmcopyutils/0.99.0

cp -L /lmod/apps/hmmcopyutils/0.99.0/bin/* .
#then edit generateMap.pl to have:
# my $fastaToRead = "$dir" . "fastaToRead";
# my $readToMap = "$dir" . "readToMap.pl";
# my $renameChr = "$dir" . "renameChr.pl";
# my $wigToBigWig = "$dir" . "wigToBigWig";

perl generateMap.pl -b genome.fa #build indexes
perl generateMap.pl genome.fa #generate mapability file

mapCounter -w 10000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_10kb.wig
mapCounter -w 20000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_20kb.wig
mapCounter -w 30000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_30kb.wig
mapCounter -w 40000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_40kb.wig
mapCounter -w 50000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_50kb.wig
mapCounter -w 60000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_60kb.wig
mapCounter -w 70000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_70kb.wig
mapCounter -w 80000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_80kb.wig
mapCounter -w 90000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_90kb.wig
mapCounter -w 100000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_100kb.wig
mapCounter -w 150000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_150kb.wig
mapCounter -w 200000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_200kb.wig
mapCounter -w 300000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_300kb.wig
mapCounter -w 400000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_400kb.wig
mapCounter -w 500000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_500kb.wig
mapCounter -w 600000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_600kb.wig
mapCounter -w 700000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_700kb.wig
mapCounter -w 800000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_800kb.wig
mapCounter -w 900000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_900kb.wig
mapCounter -w 1000000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_1000kb.wig
mapCounter -w 2000000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_2000kb.wig
mapCounter -w 5000000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_5000kb.wig
mapCounter -w 10000000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_10000kb.wig
mapCounter -w 20000000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_20000kb.wig
mapCounter -w 40000000 -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y genome.fa.map.bw > map_hg38_40000kb.wig

