ls /data/cep/Methylation/M027/ichorCNA/*Input*.wig > normalInputWigList.txt
ls /data/cep/Methylation/M031/ichorCNA/*Input*.wig >> normalInputWigList.txt
ls /data/cep/Methylation/M014/ichorCNA/*Input*.wig >> normalInputWigList.txt

Rscript bin/createPanelOfNormals.R \
--filelist normalInputWigList.txt \
--gcWig /data/cep/Methylation/refData/gc_hg38_1000kb.wig \
--mapWig /data/cep/Methylation/refData/map_hg38_1000kb.wig \
--centromere /data/cep/Methylation/refData/GRCh38.GCA_000001405.2_centromere_acen.txt \
--outfile ichorPoN_uptoM033_1000kb

